<?php

//inicializar a sessão de usuário
if (!isset($_SESSION)){
    session_start();
}


date_default_timezone_set('America/Sao_Paulo');

// inicia carregamento das classes do projeto
spl_autoload_register(function($nome_classe){
    $server_srt = $_SERVER['REQUEST_URI'];
    //verifica a origem da chamada da classe (caso venha de arquivos da pasta admin apenas usa 'class')
    //e se vier do outro caminho insere 'admin/class')
    $caminho = (strpos($server_srt, 'admin') !== false)?'class':'admin\class';
    $nome_arquivo = $caminho.DIRECTORY_SEPARATOR.$nome_classe.".php";
    if(file_exists($nome_arquivo)){
        require_once($nome_arquivo);
    }
});


//inicia carregamento das classes do projeto
spl_autoload_register(function($nome_classe){
    $nome_arquivo = "class".DIRECTORY_SEPARATOR.$nome_classe.".php";
    if(file_exists($nome_arquivo)){
        require_once($nome_arquivo);
    }
});


//Criar constantes do servidor de banco de dados
define ('IP_SERVER_DB', '127.0.0.1');
define ('HOSTNAME','ITQ0626036W10-1');
define ('NOME_BANCO','dinamico85db');
define ('USER_DB','root');
define ('PASS_DB','');


// recebe e registra imagem
function upload_imagem(){
    $foto = $_FILES['img'];
if(!empty($foto['name'])){
    $largura = 640;
    $altura = 425;
    $tamanho = 30000;
    $error = array();
    if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/",$foto['type'])){
        $error[1] = "Este arquivo não é uma imagem"; 
    }
    $dimensoes = getimagesize($foto['tmp_name']);
    if($dimensoes[0]>$largura){
        $error[2] = "A largura da imagem (".$dimensoes[0]."pixel) é maior do que a suportada (".$largura." pixel).";
    }
    $dimensoes = getimagesize($foto['tmp_name']);
    if($dimensoes[1]>$altura){
        $error[3] = "A altura da imagem (".$dimensoes[1]."pixel) é maior do que a suportada (".$altura." pixel).";
    }
    if($foto['size']>$tamanho){
        $error[4] = "A tamanho da imagem (".$foto['size']."bytes) é maior do que a suportada (".$tamanho." bytes).";
    }
    
    if(count($error) == 0){
        preg_match("/\.(gif|bmp|png|jpg){1}$/i",$foto['name'],$ext);
        $nome_img = md5(uniqid(time())).$ext[0];
        $caminho_img = "foto/".$nome_img;
        move_uploaded_file($foto['tmp_name'],$caminho_img);}
    }
        $_FILES = null;
        $imagem_info = array();
        $imagem_info[0] = $nome_img;
        $imagem_info[1] = $error;
        return $imagem_info;
}
?>
<!-- Vamos colocar para recuperar a extensão do arquivo que o usuário escolheu e enviou -->