<?php
class Categoria{
private $id;
private  $categoria;
private $ativ;

public function getId(){
return $this->id;
}
public function setId($value){
$this->id = $value;
}
public function getCategoria(){
    return $this->categoria;
}
public function setCategoria($value){
    $this->categoria = $value;
}
public function getAtiv(){
    return $this->ativ;
}
public function setAtiv($value){
    $this->ativ = $value;
}
public function loadById($_id){
    $sql = new Sql();
    $results = $sql->select('SELECT * FROM categoria WHERE id = :id', array(':id'=>$_id));
    if (count($results)>0){
        $this->setArq($results[0]);
    }
}
public static function getList(){
    $sql = new Sql();
    return $sql->select('SELECT * FROM categoria order by categoria');
}
public static function search($categoria){
    $sql = new Sql();
    return $sql->select('SELECT * FROM categoria WHERE categoria LIKE :categoria', array('categoria'=>'%'.$categoria.'$'));
}
public function setArq($arq){
    $this->setId($arq['id']);
    $this->setCategoria($arq['categoria']);
    $this->setAtiv($arq['ativ']);
}
public function insert(){
    $sql = new Sql();
    $results = $sql->select('CALL sp_categoria_insert(:categoria:ativ', array(
        ':categoria'=>$this->getCategoria(),
        ':ativ'=>$this->getAtiv()
    ));
    if (count($results)>0) {
        $this->setArq($results[0]);
    }
}
public function update($_id, $_ativ, $_categoria){
    $sql = new Sql();
    $sql->query('UPDATE categoria SET categoria=:categoria, ativ=:ativ, WHERE id=:id', array(
        ':id'=>$_id,
        ':categoria'=>$_categoria,
        ':email'=>$_ativ
    ));
    }
    public function delete(){
        $sql = new Sql();
        $sql->query("DELETE FROM categoria WHERE id_categoria = :id" ,array(":id"=>$this->getID()));
    }
    public function __construct($_categoria='', $_ativ=''){
        $this->nome = $_categoria;
        $this->email = $_ativ;
    }

}

?>