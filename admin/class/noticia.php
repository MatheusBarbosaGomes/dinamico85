<?php
  
    class Noticia
    {
        
        private $id;
        private $id_categoria;
        private $titulo;
        private $img;
        private $visita;
        private $data;
        private $ativo;
        private $noticia;
     
        public function getId()
        {
            return $this->id;
        }
        public function setId($value)
        {
            $this->id = $value;
        }
        
        public function getIdCategoria()
        {
            return $this->id_categoria;
        }        
        public function setIdCategoria($value)
        {
            $this->id_categoria = $value;
        }
        
        public function getTitulo()
        {
            return $this->titulo;
        }
        public function setTitulo($value)
        {
            $this->titulo = $value;
        }
        
        public function getImg()
        {
            return $this->img;
        }
        public function setImg($value)
        {
            $this->img = $value;
        }
        
        public function getVisita()
        {
            return $this->visita;
        }
        public function setVisita($value)
        {
            $this->visita = $value;
        }
        
        public function getData()
        {
            return $this->data;
        }
        public function setData($value)
        {
            $this->data = $value;
        }
        
        public function getAtivo()
        {
            return $this->ativo;
        }
        public function setAtivo($value)
        {
            $this->ativo = $value;
        }
        
        public function getNoticia()
        {
            return $this->noticia;
        }
        public function setNoticia($value)
        {
            $this->noticia = $value;
        }
        public function insert($_idCategoria,$_titulo,$_img,$_data,$_ativo,$_noticia)
        {
            $sql = new Sql();
            $results = $sql->select('CALL sp_noticias_insert (:categoria,:titulo,:img,0,:_data,:ativo,:noticia)',
            array(
                ':categoria'=>$_idCategoria,
                ':titulo'=>$_titulo,
                ':img'=>$_img,
                ':_data'=>$_data,
                ':ativo'=>$_ativo,
                ':noticia'=>$_noticia));
            if(count($results) > 0)
            {            
                $this->setDados($results[0]);
            }
        }
        public function updateNoticia($_id, $_idCategoria, $_titulo, $_img, $_data, $_ativo, $_noticia)
        {
            $sql = new Sql();
            $sql->query('UPDATE noticias set id_categoria = :categoria,
            titulo_noticia = :titulo,
            img_noticia = :img,
            data_noticia = :data,
            noticia_ativo = :ativo,
            noticia = :noticia where id_noticia = :id',
            array(':categoria'=>$_idCategoria,
            ':titulo'=>$_titulo,
            ':img'=>$_img,
            ':data'=>$_data,
            ':ativo'=>$_ativo,
            ':noticia'=>$_noticia,
            ':id'=>$_id));
        }
        public function updateVisita($id){
            $sql = new Sql();
            $sql->query("UPDATE noticias SET visita_noticia = visita_noticia +1 WHERE id_noticia = :id", 
            array(
                ":id"=>$id
            ));
          }
        
        public function deleteNoticia($_id)
        {
            $sql = new Sql();
            $sql->query('DELETE from noticias where id_noticia = :id',array(':id'=>$_id));
        }
        public function listarNoticias()
        {
            $sql = new Sql();
            return $sql->select('select * from noticias');
        }
        public function getList()
        {
            $sql = new Sql();
            return $sql->select('select a.id_noticia, b.categoria, a.titulo_noticia,a.img_noticia,a.visita_noticia,
            a.data_noticia, a.noticia_ativo,a.noticia from noticias a INNER JOIN categoria b on a.id_categoria = b.id_categoria');
        }
        public function consultarId($_id)
        {
            $sql = new Sql();
            return $sql->select('select * from noticias where id = :id',array(':id'=>$_id));
        }
        public function consultarTitulo($_titulo)
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM noticias where titulo_noticia LIKE :titulo',array(':titulo'=>'%'.$_titulo.'%'));
        }    
        public function __construct($_id='',$_idCategoria='',$_titulo='',$_img='',$_data='',$_ativo='',$_noticia='')
        {
            $this->id = $_id;
            $this->id_categoria = $_idCategoria;
            $this->titulo = $_titulo;
            $this->img = $_img;
            $this->data = $_data;
            $this->ativo = $_ativo;
            $this->noticia = $_noticia;
        }    
        public function setDados($dados)
        {
            $this->id = $dados['id_noticia'];
            $this->id_categoria = $dados['id_categoria'];
            $this->titulo = $dados['titulo_noticia'];
            $this->img = $dados['img_noticia'];
            $this->visita = $dados['visita_noticia'];
            $this->data = $dados['data_noticia'];
            $this->ativo = $dados['noticia_ativo'];
            $this->noticia = $dados['noticia'];
        }
    }
?>