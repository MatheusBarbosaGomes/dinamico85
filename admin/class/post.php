<?php
    class Post
    {
        private $id;
        private $idCategoria;
        private $titulo;
        private $descricao;
        private $img;
        private $visitas;
        private $data;
        private $ativo;

        public function getId()
        {
            return $this->id;
        }
        public function setId($value)
        {
            $this->id = $value;
        }
        public function getIdCategoria()
        {
            return $this->idCategoria;
        }
        public function setIdCategoria($value)
        {
            $this->idCategoria = $value;
        }
        public function getTitulo()
        {
            return $this->titulo;
        }
        public function setTitulo($value)
        {
            $this->titulo = $value;
        }
        public function getDescricao()
        {
            return $this->descricao;
        }
        public function setDescricao($value)
        {
            $this->descricao = $value;
        }
        public function getImg()
        {
            return $this->img;
        }
        public function setImg($value)
        {
            $this->img = $value;
        }
        public function getVisitas()
        {
            return $this->visitas;
        }
        public function setVisitas($value)
        {
            $this->visitas = $value;
        }
        public function getData()
        {
            return $this->data;
        }
        public function setData($value)
        {
            $this->data = $value;
        }
        public function getAtivo()
        {
            return $this->ativo;
        }
        public function setAtivo($value)
        {
            $this->ativo = $value;
        }
        public function inserirPost($_idCategoria,$_titulo,$_descricao,$_img,$_data,$_ativo)
        {
            $sql = new Sql();
            $results = $sql->select('CALL sp_post_insert(:idcategoria,:titulo,:descricao,:img,0,:data_post,:ativo)',
            array(
                ':idcategoria'=>$_idCategoria,
                ':titulo'=>$_titulo,
                ':descricao'=>$_descricao,
                ':img'=>$_img,
                ':data_post'=>$_data,
                ':ativo'=>$_ativo));
            if(count($results)>0)
            {
                $this->setDados($results[0]);
            }
            
        }
        public function updatePost($_id,$_idCategoria,$_titulo,$_descricao,$_img,$_visitas,$_data,$_ativo)
        {
            $sql = new Sql();
            $sql->query('UPDATE post set id_categoria = :id_categoria ,titulo_post = :titulo,descricao_post = :descricao,img_post = :img
            ,visitas = :visitas,data_post = :data_post,post_ativo = :ativo, visitas = :visitas where id_post = :id',
            array(
                ':id_categoria'=>$_idCategoria,
                ':titulo'=>$_titulo,
                ':descricao'=>$_descricao,
                ':img'=>$_img,
                ':data_post'=>$_data,
                ':ativo'=>$_ativo,
                ':id'=>$_id,
                ':visitas'=>$_visitas));
        }
        public function deletePost($_id)
        {
            $sql = new Sql();
            $sql->query('DELETE FROM post where id_post = :id',array(':id'=>$_id));
        }
        public function consultarIdPost($_id)
        {
            $sql = new Sql();
            return $sql->select('select * from post where id_post = :id',array(':id'=>$_id));
        }
        public function consultarTituloPost($_titulo)
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM post where titulo_post LIKE :titulo',array(':titulo'=>'%'.$_titulo.'%'));
        }
        public function consultarDescricaoPost($_descricao)
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM post where descricao_post LIKE :descricao',array(':descricao'=>'%'.$_descricao.'%'));
        }
        public function listarPost()
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM post');
        }
        public function listarPostInner()
        {
            $sql = new Sql();
            return $sql->select('select a.id_post, b.categoria, a.titulo_post, a.descricao_post,a.img_post,a.visitas,
            a.data_post, a.post_ativo from post a INNER JOIN categoria b on a.id_categoria = b.id_categoria');
        }
        public function __construct($_id='',$_idCategoria='',$_titulo='',$_descricao='',$_img='',$_visitas='',$_data='',$_ativo='')
        {
            $this->id = $_id;
            $this->idCategoria = $_idCategoria;
            $this->titulo = $_titulo;
            $this->descricao = $_descricao;
            $this->img = $_img;
            $this->visitas = $_visitas;
            $this->data = $_data;
            $this->ativo = $_ativo;
        }

        public function setDados($dados)
        {
            $this->id = $dados['id_post'];
            $this->idCategoria = $dados['id_categoria'];
            $this->titulo = $dados['titulo_post'];
            $this->descricao = $dados['descricao_post'];
            $this->img = $dados['img_post'];
            $this->visitas = $dados['visitas'];
            $this->data = $dados['data_post'];
            $this->ativo = $dados['post_ativo'];
        }
    }
?>