<?php
$id_noticia= filter_input(INPUT_GET,'id_noticia');
$categoria= filter_input(INPUT_GET,'categoria');
$titulo_noticia= filter_input(INPUT_GET,'titulo_noticia');
$img_noticia= filter_input(INPUT_GET,'img_noticia');
$visita_noticia= filter_input(INPUT_GET,'visita_noticia');
$data_noticia= filter_input(INPUT_GET,'data_noticia');
$noticia_ativo= filter_input(INPUT_GET,'noticia_ativo');
$noticia= filter_input(INPUT_GET,'noticia');

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Alterar Noticia</title>
</head>
<body>
    <form action="op_administrador.php" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>Alterar Noticia</legend>
            <div>
                <input type="hidden" name="id" value="<?php echo $id; ?>">
            </div>
            <div>
              <label for="">Titulo</label>  
              <input type="text" name="titulo" value="<?php echo $titulo_noticia; ?>">
            </div>
            <div>
              <label for="">Data</label>  
              <input type="text" name="data" value="<?php echo $data_noticia; ?>">
            </div>
            <div>
              <label for="">Imagem</label>  
              <input type="text" name="img" value="<?php echo $img_noticia; ?>">
            </div>
            <div>
                <input type="submit" name="alterar" value="Registrar Alteração">
            </div>
        </fieldset>
    </form>
</body>
</html>