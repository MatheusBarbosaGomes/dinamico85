<?php
    require_once('../config.php');        
    if(count($categorias_retornadas = Categoria::getList())> 0 )
    {
    
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Lista Categoria</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_categoria" width='100%' border="0" cellpadding="0" cellspacing="1" bgcolor="">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2"><font size="2" color="#fff">Código</font></th>
            <th width="60%" height="2"><font size="2" color="#fff">Categoria</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Ativo</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php
            foreach($categorias_retornadas as $categoria)
            {
        ?>
        <tr>
            <td><font size="2" face="verdana, arial" color="#000"><?php echo $categoria['id_categoria']?></font></td>
            <td><font size="2" face="verdana, arial" color="#000"><?php echo $categoria['categoria']?></font></td>
            <td><font size="2" face="verdana, arial" color="#000"><?php echo $categoria['cat_ativo']==1?'Sim':'Não'?></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000"></font><a href="principal.php?link=">Alterar</a></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000"></font><a href="principal.php?link=">Excluir</a></td>
        </tr>
        <?php
            }}
        ?>
    </table>
</body>
</html>