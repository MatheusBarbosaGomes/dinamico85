<?php
require_once('../config.php');
if(isset($_POST['cadastro'])){
    $user = new Usuario($_POST['nome'],$_POST['email'],
    $_POST['login'],$_POST['senha']);

    $user->insert();
    if($user->getId()!=null){
        header('location:principal.php?link=3&msg=ok');
    }
}
    $id = filter_input(INPUT_GET,'id');
    $excluir = filter_input(INPUT_GET,'excluir');
    if(isset($id)&& $excluir==1){
        $user = new Usuario();
        $user->setId($id);
        $user->delete();
        header('location:principal.php?link=9&msg=ok');
    }
    if(isset($_POST['alterar'])){
        $user = new Usuario();
        $user->update($_POST['id'], $_POST['nome'], $_POST['email'],
        $_POST['login']);
        header('location:principal.php?link=3&msg=ok');
    }
    $txt_login = isset($_POST['txt_login'])?$_POST['txt_login']:'';
    $txt_senha = isset($_POST['txt_senha'])?$_POST['txt_senha']:'';

    if(empty($txt_login) || empty($txt_senha))  {
        header('Location:  index.php?msg=Preencha os dados do Usuário');
        exit;
    }
    $_SESSION['logado'] = true;
    $_SESSION['id_user'] = $user->getId();
    $_SESSION['nome_user'] = $user->getNome();
    $_SESSION['login_user'] = $user->getLogin();
    header('Location: principal.php?link=');

    if($_GET['sair']){
    $_SESSION['logado'] = false;
    $_SESSION['id_user'] = null;
    $_SESSION['nome_user'] = null; 
    $_SESSION['login_user'] = null;
    header('Location: index.php');    
}

?>