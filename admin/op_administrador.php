<?php
require_once('../config.php');
//Inserir ADM

if(isset($_POST['cadastro'])){
  $adm = new Administrador(
    $_POST['nome'],
    $_POST['email'],
    $_POST['login'],
    $_POST['senha']
  );
  
$adm->insert();
  if($adm->getId()!=null){
    header('location:principal.php?link=9&msg=ok');
  }
}
//excluiu ou deletar o ADMINISTRADOR;
$id = filter_input(INPUT_GET,'id');
$excluir = filter_input(INPUT_GET,'excluir');
if(isset($id)&& $excluir==1){
 $admin = new Administrador();
 $admin->setId($id);
 $admin->delete();
 header('location:principal.php?link=9&msg=ok');
}
//Alterar o Administrador; 
if(isset($_POST['alterar'])){
  $adm = new Administrador();
$adm->update($_POST['id'], $_POST['nome'],$_POST['email'],$_POST['login']);
    header('location:principal.php?link=9&msg=ok');
  
}
$txt_login = isset($_POST['txt_login'])?$_POST['txt_login']:'';
$txt_senha = isset($_POST['txt_senha'])?$_POST['txt_senha']:'';
//echo $txt_login.' - '.$txt_senha;
if(empty($txt_login) || empty($txt_senha))  {
    header('Location: index.php?msg=Preencha os dados de Usuário'); 
    exit;
}
$adm = new Administrador();

$adm->efetuarlogin($txt_login,$txt_senha); 
if(($adm->getId()==null))  {
    // echo $adm->getId();
    header('Location: index.php?msg=Usuário ou Senha Incorretos');
    exit;
}

// registrando sessão do usuário
$_SESSION['logado'] = true;
$_SESSION['id_adm'] = $adm->getId();
$_SESSION['nome_adm'] = $adm->getNome();
$_SESSION['login_adm'] = $adm->getLogin();
header('Location: principal.php?link=');

// encerrando a sessão de usuário
if($_GET['sair']){
$_SESSION['logado'] = false;
$_SESSION['id_adm'] = null;
$_SESSION['nome_adm'] = null;
$_SESSION['login_adm'] = null;
header('Location: index.php');
}


?>