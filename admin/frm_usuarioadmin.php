<?php
require_once('conexao.php');
if(isset($_POST['cadastro_usuario'])){
    $nome=$_POST['nome'];
    $email=$_POST['email'];
    $foto=$_FILES['foto'];
// ajustando o tamanho largura e altura das imagens
// e escolhendo os tipos de imagens que irá suportar
    if(!empty($foto['name'])){
        $largura = 640;
        $altura = 425;
        $tamanho = 30000;
        $error = array();
        if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/",$foto['type'])){
            $error[1] = "Este arquivo não é uma imagem"; 
        }
        $dimensoes = getimagesize($foto['tmp_name']);
        if($dimensoes[0]>$largura){
            $error[2] = "A largura da imagem (".$dimensoes[0]."pixel) é maior do que a suportada (".$largura." pixel).";
        }
        $dimensoes = getimagesize($foto['tmp_name']);
        if($dimensoes[1]>$altura){
            $error[3] = "A altura da imagem (".$dimensoes[1]."pixel) é maior do que a suportada (".$altura." pixel).";
        }
        if($foto['size']>$tamanho){
            $error[4] = "A tamanho da imagem (".$foto['size']."bytes) é maior do que a suportada (".$tamanho." bytes).";
        }
        // Vamos colocar para recuperar a extensão do arquivo que o usuário escolheu e enviou
        if(count($error)==0){
            preg_match("/\.(gif|bmp|png|jpg){1}$/i",$foto['name'],$ext);
            $nome_img = md5(uniqid(time())).$ext[0];
            $caminho_img = "foto/".$nome_img;
            //fazer um upload de imagem e guardar no servidor temporariamente
            move_uploaded_file($foto['tmp_name'],$caminho_img);
            $cmd = $cn->prepare("insert into usuario (nome, email, foto) values (:nome, :email, :foto)");
            $result = $cmd->execute(array(
                ":nome"=>$nome,
                ":email"=>$email,
                ":foto"=>$nome_img
            ));
            if ($result==true){
                echo "<br> Usuário inserido com Sucesso.";
            }
            if (count($error)!=0){
                foreach ($error as $erro){
                    echo $erro."<br>";
                }
            }
        }
    }

}


?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Cadastro de Usuário</title>
</head>
<body>
    <h1>Novo Usuário</h1>
    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" name="cadastro_form">
    Nome:<br>
    <input type="text" name="nome" class="form-control"><br><br>
    Email:<br>
    <input type="text" name="email" class="form-control"><br><br>
    Foto de Exibição:<br>
    <input type="file" name="foto" class="form-control"><br><br>
    <input type="submit" name="cadastro_usuario" value="Cadastrar Usuário" class="btn btn-success"><br><br>
     
    </form>
    <br>
    <hr>
    <h2>Usuários Cadastros</h2>

    <?php
        $cmd = $cn->prepare("select * from usuario");
        $cmd->execute();
        $result = $cmd->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $usuario){
            echo "<img src= 'foto/".$usuario['foto']."'width='56' height='48'>";
            echo "<br>";
            echo "<b>Nome:</b> ".$usuario['nome'];
            echo "<br>";
            echo "<b>Email:</b> ".$usuario['email'];
            echo "<br>";
        }
    ?>

</body>
</html>